var _ = require('underscore'),
    Q = require('q'),
    github = require('./github'),
    db = require('./db'),
    mail = require('./mail'),
    config = require('../config');

var client = github(config.githubUsername, config.githubPassword);

var getFulfilledValues = function(promises) {
    var fulfilled = _.where(promises, { state: 'fulfilled' });
    return _.pluck(fulfilled, 'value');
};

var checkForWatchedFiles = function() {

    db.query('select owner, repo from watchers group by repo').then(
        function (watchedRepos) {
            return client.repoToFilenameMap(watchedRepos);
        }
    ).then(
        function (filesByPullRequestInRepo) {
            var openPullRequestsByRepo = _.extend.apply(_, getFulfilledValues(filesByPullRequestInRepo));
            return [openPullRequestsByRepo, db.query('select * from watchers')];
        }
    ).spread(
        function (openPullRequestsByRepo, watchers) {
            var promises = [];

            _.each(watchers, function(watcher, i) {
                var repo = watcher.owner + '/' + watcher.repo,
                    pullRequests = openPullRequestsByRepo[repo];

                if (pullRequests) {
                    _.each(pullRequests, function(pr, i) {
                        _.each(pr.files, function(file, i) {
                            if (file.indexOf(watcher.pattern) > -1) {
                                var alert = db.query('insert into alerts (watcherId, pullRequest) VALUES(?, ?)',
                                    [watcher.id, pr.number]
                                );

                                promises.push(alert);
                            }
                        });
                    });
                }
            });

            return Q.allSettled(promises);
        }
    ).then(
        function (insertedAlerts) {
            insertedAlerts = getFulfilledValues(insertedAlerts);

            if (!insertedAlerts.length) {
                return [];
            }

            return db.query('select a.*, w.username as username, w.owner as owner, ' +
                        'w.repo as repo, w.pattern as pattern, u.email as email from alerts a ' +
                    'JOIN watchers w ON a.watcherId = w.id ' +
                    'JOIN users u ON w.username = u.username ' +
                    'where a.id IN (?) and a.notifiedAt is null',
                [_.pluck(insertedAlerts, 'insertId')]
            );
        }
    ).then(
        function (alertsToBeNotified) {
            if (!alertsToBeNotified.length) {
                return [];
            }

            var promises = [];

            _.each(alertsToBeNotified, function(alert, i) {
                promises.push(mail.sendAlert(alert));
            });

            return Q.allSettled(promises);
        }
    ).then(
        function (sentAlertMessages) {
            sentAlertMessages = getFulfilledValues(sentAlertMessages);

            if (!sentAlertMessages.length) {
                return [];
            }

            var ids = _.reduce(sentAlertMessages, function(memo, message, i) {
                memo.push(message.alert.id);
                return memo;
            }, []);

            return db.query('update alerts set notifiedAt = CURRENT_TIMESTAMP where id IN (?)',
                [ids]
            );
        }
    ).then(
        function (updatedAlerts) {}
    ).catch(
        function (error) {
            console.log(error);
        }
    ).done();
}

module.exports = exports = {
    startWithInterval: function(interval) {
        checkForWatchedFiles();

        setInterval(function() {
            checkForWatchedFiles();
        }, interval);
    }
};