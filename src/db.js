var mysql = require('mysql'),
    Q = require('q'),
    config = require('../config');

var db = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    port: config.db.port,
    database: config.db.database
});

db.connect();

module.exports = exports = {
    query: function(statement, params) {
        var def = Q.defer();

        db.query(statement, params, function(err, result) {
            if (err) {
                return def.reject(err);
            }

            def.resolve(result);
        });

        return def.promise;
    }
};