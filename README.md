### Gitwatch

Keep an eye on files and folders across your organization.

![UI Example](https://cloud.githubusercontent.com/assets/185070/6890261/c43381ae-d675-11e4-8828-62af4be1ed4b.png)

#### Why?

As companies grow, code ownership changes and individuals rotate across teams. `git blame` is
great for finding who originally authored a line, but it becomes less useful in fast-moving
codebases.

Perhaps you want to watch a service evolve, but only the HTTP interface.

Perhaps you are changing teams and want to keep an eye on code with new owners.

Perhaps a module in another team's code ownership boundary is critical to your team (ie analytics).

#### Get Started

1. Run `npm install`.
2. Enter your real credentials in `config.example.js`. Then rename it to `config.js`.
3. Run `npm start`.
4. Point your browser to [http://localhost:3001/](http://localhost:3001/).