var Q = require('q'),
    db = require('../db'),
    github = require('../github'),
    config = require('../../config'),
    ensureAuthenticated = require('./middleware/ensureAuthenticated');

var client = github(config.githubUsername, config.githubPassword);

module.exports = exports = function(app) {
    app.get('/', ensureAuthenticated, function(req, res){
        Q.all([
            client.accessibleRepos(req.user.username),
            db.query('select * from watchers where username = ? and removedAt is null order by createdAt ASC', [req.user.username])
        ]).then(function(response) {
            res.render('create', {
                username: req.user.username,
                repos: response[0],
                watchers: response[1]
            });
        });
    });
};