var request = require('request'),
    _ = require('underscore'),
    Q = require('q');

module.exports = exports = function(username, password) {

    if (!username || !password) {
        throw Error('Initialize Github API client with a username and password');
    }

    var client = function(url) {
        var def = Q.defer(),
            authToken = new Buffer(username + ':' + password).toString('base64');

        request({
            url: 'https://api.github.com/' + url,
            headers: {
                'User-Agent': 'gitwatch',
                'Authorization': 'Basic ' + authToken
            }
        }, function (error, response, body) {
          if (!error && response.statusCode == 200) {
            def.resolve(JSON.parse(response.body));
            return;
          }

          def.reject(error);
        });

        return def.promise;
    };

    return {
        fetch: function(url) {
            return client(url);
        },
        pullsInRepo: function(owner, repo) {
            return client('repos/' + owner + '/' + repo + '/pulls');
        },
        filesInPull: function(owner, repo, pullNumber) {
            return client('repos/' + owner + '/' + repo + '/pulls/' + pullNumber + '/files');
        },
        filesInPullWithNumber: function(owner, repo, pullNumber) {
            var def = Q.defer();

            this.filesInPull(owner, repo, pullNumber).then(function (files) {
                def.resolve({
                    number: pullNumber,
                    files: files
                });
            });

            return def.promise;
        },
        filenamesInRepoByPullRequest: function(owner, repo) {
            var def = Q.defer();

            this.pullsInRepo(owner, repo).then(function(pulls) {
                var pullPromises = [];

                for(var i = 0; i < pulls.length; i++) {
                    pullPromises.push(this.filesInPullWithNumber(owner, repo, pulls[i].number));
                }

                Q.all(pullPromises).then(function(filesWithNumber) {
                    _.each(filesWithNumber, function(el, i) {
                        var allFilenames = _.pluck(el.files, 'filename');
                        el.files = allFilenames;
                    });

                    var map = {};
                    map[owner + '/' + repo] = filesWithNumber;

                    def.resolve(map);
                }, function() {
                    def.reject('failed to fetch all files for all pulls in: ' + owner + '/' + repo);
                });
            }.bind(this), function() {
                def.reject('failed to fetch pulls in: ' + owner + '/' + repo);
            });

            return def.promise;
        },
        repoToFilenameMap: function(ownersAndRepos) {
            var promises = [];

            _.each(ownersAndRepos, function(el, i) {
                promises.push(this.filenamesInRepoByPullRequest(el.owner, el.repo));
            }.bind(this));

            return Q.allSettled(promises); // 'Betterment/better-core': {number: 10, files: 'aoasidnf.ofin', ...] }
        },
        reposInUserOrgs: function(username) {
            var def = Q.defer();

            this.fetch('users/' + username + '/orgs').then(function(results) {
                var promises = [];

                for (var i = 0; i < results.length; i++) {
                    var org = results[i];
                    promises.push(client('orgs/' + org.login + '/repos'));
                }

                Q.all(promises).then(function(repos) {
                    def.resolve(repos[0]);
                });
            });

            return def.promise;
        },
        accessibleRepos: function(username) {
            var def = Q.defer();

            Q.all([
                this.fetch('users/' + username + '/repos'),
                this.reposInUserOrgs(username)
            ]).then(function(response) {
                var repos = _.reduce(response, function(memo, el, i) {
                    return memo.concat(el);
                }, []);

                def.resolve(repos);
            });

            return def.promise;
        }
    }
};