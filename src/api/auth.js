var config = require('../../config'),
    GitHubStrategy = require('passport-github').Strategy;

module.exports = exports = function(app, passport) {
    passport.serializeUser(function(user, done) {
      done(null, user);
    });

    passport.deserializeUser(function(obj, done) {
      done(null, obj);
    });

    // Use the SoundCloudStrategy within Passport.
    //   Strategies in Passport require a `verify` function, which accept
    //   credentials (in this case, an accessToken, refreshToken, and SoundCloud
    //   profile), and invoke a callback with a user object.
    passport.use(new GitHubStrategy({
        clientID: config.clientID,
        clientSecret: config.clientSecret,
        callbackURL: config.callbackURL
      },
      function(accessToken, refreshToken, profile, done) {
        return done(null, profile);
      }
    ));

    // The first step in SoundCloud authentication will involve
    // redirecting the user to soundcloud.com.  After authorization, SoundCloud
    // will redirect the user back to this application at
    // /auth/soundcloud/callback
    app.get('/auth/github', passport.authenticate('github'), function(){});

    // If authentication fails, the user will be redirected back to the
    // login page.  Otherwise, the primary route function function will be called,
    // which, in this example, will redirect the user to the home page.
    app.get('/auth/github/callback',
      passport.authenticate('github', { failureRedirect: '/login' }),
      function(req, res) {
        res.redirect('/');
      });

    app.get('/login', function(req, res) {
        res.render('index');
    });

    app.get('/logout', function(req, res) {
      req.logout();
      res.redirect('/login');
    });
};