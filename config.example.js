module.exports = exports = {
    githubUsername: 'foo_programmer',
    githubPassword: 'SECRET',

    // Github application details
    clientID: 'SECRET',
    clientSecret: 'SECRET',
    callbackURL: 'http://localhost:3001/auth/github/callback',

    gmailUsername: 'user@example.org',
    gmailPassword: 'SECRET',

    db: {
        host: '127.0.0.1',
        user: 'root',
        password: 'SECRET',
        port: 3306,
        database: 'gitwatch'
    }
};