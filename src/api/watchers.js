var db = require('../db'),
    ensureAuthenticated = require('./middleware/ensureAuthenticated');

module.exports = exports = function(app, helpers) {
    app.get('/api/watchers', ensureAuthenticated, function(req, res) {
        db.query('select * from watchers where username = ? and removedAt is null',
            [req.user.username]
        ).then(
            function success(response) {
                var status = !response ? 204 : 200;
                res.status(status).send(response);
            },
            function error(error) {
                res.status(500).send(error);
            }
        );
    });

    app.post('/api/watchers', ensureAuthenticated, function(req, res) {
        var body = req.body;

        // TODO: check access to `owner`? Check access to `repo`?
        // Check that pattern is a string (or regex?)
        // String trailing and leading whitespace from owner, repo, and pattern?
        if (!body.owner || !body.repo || !body.pattern) {
            return res.status(400).send({
                error: 'Must provide repo and pattern.'
            });
        }

        db.query('insert into watchers (username, owner, repo, pattern) VALUES(?, ?, ?, ?)',
            [req.user.username, body.owner, body.repo, body.pattern]
        ).then(
            function success(response) {
                db.query('select * from watchers where id = ?', [response.insertId]).then(
                    function success(response) {
                        res.send(response[0]);
                    },
                    function error() {
                        res.status(500).send(error);
                    }
                );
            },
            function error(error) {
                if (error.code === 'ER_DUP_ENTRY') {
                    return res.status(409).send({
                        error: 'A watcher with those parameters already exists.'
                    });
                }

                res.status(500).send(error);
            }
        );
    });

    app.delete('/api/watchers/:id', ensureAuthenticated, function(req, res){
        var watcherId = parseInt(req.params.id, 10);

        db.query('select * from watchers where username = ? and id = ?',
            [req.user.username, watcherId]
        ).then(
            function success(response) {
                if (!response.length) {
                    return res.status(401).send({
                        error: 'You are not authorized to delete that watcher.'
                    });
                }

                db.query('update watchers set removedAt = CURRENT_TIMESTAMP where id = ?',
                    [watcherId]
                ).then(
                    function success() {
                        res.status(204).send();
                    },
                    function error(error) {
                        res.status(500).send(error);
                    }
                );
            },
            function error(error) {
                res.status(500).send(error);
            }
        );
    });
};