var nodemailer = require('nodemailer'),
    Q = require('q'),
    _ = require('underscore'),
    config = require('../config');

var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: config.gmailUsername,
        pass: config.gmailPassword
    }
});

var send = function(options, alert) {
    var def = Q.defer();

    transporter.sendMail(options, function(error, info) {
        if (error) {
            def.reject(error);
        }

        def.resolve(_.extend({}, info, {
            alert: alert
        }));
    });

    return def.promise;
};

module.exports = exports = {
    sendAlert: function(el) {
        return send({
            from: 'Gitwatch <info@gitwatch.io>',
            to: el.email,
            subject: 'Gitwatch Alert: Match found for "' + el.pattern + '" in ' + el.owner + '/' + el.repo,
            text: 'A file matching your "' + el.pattern + '" pattern has changed in: https://github.com/' + el.owner + '/' + el.repo + '/pull/' + el.pullRequest,
            html: 'A file matching your <b>"' + el.pattern + '"</b> pattern has changed in: https://github.com/' + el.owner + '/' + el.repo + '/pull/' + el.pullRequest
        }, el);
    }
};
