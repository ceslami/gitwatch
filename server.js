var _ = require('underscore'),
    express = require('express'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    passport = require('passport'),
    monitor = require('./src/monitor'),
    db = require('./src/db'),
    ensureAuthenticated = require('./src/api/middleware/ensureAuthenticated');

var routes = {
    ui: require('./src/api/ui'),
    auth: require('./src/api/auth'),
    alerts: require('./src/api/alerts'),
    watchers: require('./src/api/watchers')
};

var app = express();
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use(session({secret: 'YXNvZG5mb2Fpc25kZg=='}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());

routes.auth(app, passport);
routes.ui(app);
routes.watchers(app);
routes.alerts(app);

monitor.startWithInterval(1000*60*3);

app.listen(3001);
