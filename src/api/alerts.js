var db = require('../db'),
    ensureAuthenticated = require('./middleware/ensureAuthenticated');

module.exports = exports = function(app, helpers) {
    app.get('/api/alerts', ensureAuthenticated, function(req, res) {
        db.query('select a.* from alerts a JOIN watchers w on w.username = ? and a.watcherId = w.id',
            [req.user.username]
        ).then(
            function success(response) {
                var status = !response ? 204 : 200;
                res.status(status).send(response);
            },
            function error(error) {
                res.status(500).send(error);
            }
        );
    });
};